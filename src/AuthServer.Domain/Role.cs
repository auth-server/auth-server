﻿using System;

namespace AuthServer.Domain
{
    public class Role
    {
        public Guid Id { get; set; }
        public string? Name { get; set; }
        public string? NormalizedName { get; set; }
    }
}