﻿using System;
using FluentMigrator.Runner;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace AuthServer.Infrastructure.Extensions
{
    public static class IHostExtensions
    {
        public static void RunWithMigrations(this IHost host)
        {
            var configuration = host.Services.GetService<IConfiguration>();
            var migrateValue = configuration.GetValue<bool?>("migrate");

            if (migrateValue.HasValue)
                host.Migrate(!migrateValue.Value);
            else
                host.Run();
        }

        private static void Migrate(this IHost host, bool dryRun)
        {
            using var scope = host.Services.CreateScope();
            var migrationRunner = scope.ServiceProvider.GetRequiredService<IMigrationRunner>();

            if (dryRun)
                migrationRunner.ListMigrations();
            else
                migrationRunner.MigrateUp();
        }
    }
}