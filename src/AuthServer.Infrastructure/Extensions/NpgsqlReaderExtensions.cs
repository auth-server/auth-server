﻿using Npgsql;

namespace AuthServer.Infrastructure.Extensions
{
    public static class NpgsqlReaderExtensions
    {
        public static string? GetNullableString(this NpgsqlDataReader reader, int ordinal)
        {
            return reader.IsDBNull(ordinal)
                ? null
                : reader.GetFieldValue<string>(ordinal);
        }
    }
}