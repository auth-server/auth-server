﻿using System.Threading;
using System.Threading.Tasks;
using Npgsql;

namespace AuthServer.Infrastructure
{
    public interface INpgsqlConnectionFactory
    {
        Task<NpgsqlConnection> CreateAsync(CancellationToken cancellationToken);
    }
}