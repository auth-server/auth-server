﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Npgsql;

namespace AuthServer.Infrastructure
{
    public class NpgsqlConnectionFactory : INpgsqlConnectionFactory
    {
        private readonly IOptions<NpgsqlOptions> _options;

        public NpgsqlConnectionFactory(IOptions<NpgsqlOptions> options)
        {
            _options = options;
        }

        public async Task<NpgsqlConnection> CreateAsync(CancellationToken cancellationToken)
        {
            var connection = new NpgsqlConnection(_options.Value.ConnectionString);
            await connection.OpenAsync(cancellationToken);
            return connection;
        }
    }
}