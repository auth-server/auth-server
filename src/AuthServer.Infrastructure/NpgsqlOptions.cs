﻿namespace AuthServer.Infrastructure
{
    public class NpgsqlOptions
    {
        public string ConnectionString { get; set; } = null!;
    }
}