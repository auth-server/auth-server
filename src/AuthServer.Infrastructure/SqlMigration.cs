﻿using System;
using FluentMigrator;
using FluentMigrator.Expressions;
using FluentMigrator.Infrastructure;

namespace AuthServer.Infrastructure
{
    public abstract class SqlMigration : IMigration
    {
        private IMigrationContext _context = null!;

        public object ApplicationContext => throw new NotImplementedException();
        public string ConnectionString => _context.Connection;
        
        protected abstract string GetUpSql(IServiceProvider serviceProvider);
        protected abstract string GetDownSql(IServiceProvider serviceProvider);

        public void GetUpExpressions(IMigrationContext context)
        {
            _context = context;
            context.Expressions.Add(new ExecuteSqlStatementExpression { SqlStatement = GetUpSql(context.ServiceProvider)});
        }

        public void GetDownExpressions(IMigrationContext context)
        {
            _context = context;
            context.Expressions.Add(new ExecuteSqlStatementExpression { SqlStatement = GetDownSql(context.ServiceProvider)});
        }
    }
}