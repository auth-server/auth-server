﻿using System;
using AuthServer.Infrastructure;
using FluentMigrator;
using FluentMigrator.Builders.Execute;
using FluentMigrator.Infrastructure;
using FluentMigrator.Runner;

namespace AuthServer.Persistence.Migrations
{
    [Migration(1, "Initial")]
    public class Initial : SqlMigration
    {
        protected override string GetUpSql(IServiceProvider serviceProvider) => $@"
CREATE TABLE users(
    id                      uuid    NOT NULL PRIMARY KEY,
    user_name               text    NOT NULL,
    normalized_user_name    text    NOT NULL,
    email                   text    NOT NULL,
    normalized_email        text    NOT NULL,
    email_confirmed         boolean NOT NULL DEFAULT FALSE,
    phone_number            text        NULL,
    phone_number_confirmed  boolean NOT NULL DEFAULT FALSE,
    password_hash           text    NOT NULL
);

CREATE UNIQUE INDEX normalized_user_name_unique_idx on users (normalized_user_name);
CREATE UNIQUE INDEX normalized_email_unique_idx on users (normalized_email);

CREATE TABLE roles(
    id              uuid NOT NULL PRIMARY KEY,
    name            text NOT NULL,
    normalized_name text NOT NULL    
);

CREATE TABLE user_roles(
    user_id uuid,
    role_id uuid,

    CONSTRAINT user_roles_pk PRIMARY KEY(user_id, role_id)
);
";

        protected override string GetDownSql(IServiceProvider serviceProvider) => $@"
DROP TABLE users;
DROP TABLE roles;
DROP TABLE user_roles;
";
    }
}