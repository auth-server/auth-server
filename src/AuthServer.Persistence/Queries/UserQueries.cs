﻿namespace AuthServer.Persistence.Queries
{
    public static class UserQueries
    {
        public const string FindByIdQuery = @"
SELECT id, user_name, email, phone_number, password_hash
FROM users
WHERE id = :id
";
        
        public const string FindByNameQuery = @"
SELECT id, user_name, email, phone_number, password_hash
FROM users
WHERE normalized_user_name = :normalized_user_name
";
        
        public const string FindByEmailQuery = @"
SELECT id, user_name, email, phone_number, password_hash
FROM users
WHERE normalized_email = :normalized_email
";

        public const string Exists = @"
SELECT EXISTS (
    SELECT 1 FROM USERS
    WHERE (:normalized_user_name IS NULL OR normalized_user_name = :normalized_user_name) AND
    (:normalized_email IS NULL OR normalized_email = :normalized_email));
";

        public const string Create = @"
INSERT INTO users(id, user_name, normalized_user_name, email, normalized_email, password_hash)
VALUES(:id, :user_name, :normalized_user_name, :email, :normalized_email, :password_hash)
RETURNING id;
";

        public const string Update = @"
UPDATE users
SET user_name = :user_name,
    normalized_user_name = :normalized_user_name,
    email = :email,
    normalized_email = :normalized_email,
    phone_number = :phone_number,
    phone_number_confirmed = :phone_number_confirmed,
    password_hash = :password_hash
WHERE id = :id;
";

        public const string Delete = @"
DELETE FROM users
WHERE id = :id;
";
    }
}