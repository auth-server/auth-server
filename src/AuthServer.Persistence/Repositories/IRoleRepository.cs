﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AuthServer.Domain;

namespace AuthServer.Persistence.Repositories
{
    public interface IRoleRepository
    {
        Task<Role> FindByIdOrDefaultAsync(Guid id, CancellationToken cancellationToken);
        Task<Role> FindByNameOrDefaultAsync(string normalizedName, CancellationToken cancellationToken);
        
        Task<Guid> CreateAsync(Role role, CancellationToken cancellationToken);
        Task UpdateAsync(Role role, CancellationToken cancellationToken);
        Task DeleteAsync(Guid id, CancellationToken cancellationToken);
    }
}