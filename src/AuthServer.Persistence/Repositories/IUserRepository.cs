﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AuthServer.Domain;
using AuthServer.Users;

namespace AuthServer.Persistence.Repositories
{
    public interface IUserRepository
    {
        Task<User?> FindByIdOrDefaultAsync(Guid id, CancellationToken cancellationToken);
        Task<User?> FindByNameOrDefaultAsync(string normalizedUserName, CancellationToken cancellationToken);
        Task<User?> FindByEmailOrDefaultAsync(string normalizedEmail, CancellationToken cancellationToken);

        Task<bool> ExistsAsync(UserFilter userFilter, CancellationToken cancellationToken);
        
        Task<Guid?> CreateAsync(User user, CancellationToken cancellationToken);
        Task UpdateAsync(User user, CancellationToken cancellationToken);
        Task DeleteAsync(Guid id, CancellationToken cancellationToken);
    }
}