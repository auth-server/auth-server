﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AuthServer.Domain;

namespace AuthServer.Persistence.Repositories
{
    public class RoleRepository : IRoleRepository
    {
        public Task<Role> FindByIdOrDefaultAsync(Guid id, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<Role> FindByNameOrDefaultAsync(string normalizedName, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task<Guid> CreateAsync(Role role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(Role role, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(Guid id, CancellationToken cancellationToken)
        {
            throw new NotImplementedException();
        }
    }
}