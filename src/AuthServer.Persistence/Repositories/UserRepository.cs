﻿using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using AuthServer.Domain;
using AuthServer.Infrastructure;
using AuthServer.Infrastructure.Extensions;
using AuthServer.Persistence.Queries;
using AuthServer.Users;
using Npgsql;

namespace AuthServer.Persistence.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly INpgsqlConnectionFactory _connectionFactory;

        public UserRepository(INpgsqlConnectionFactory connectionFactory)
        {
            _connectionFactory = connectionFactory;
        }

        public async Task<User?> FindByIdOrDefaultAsync(Guid id, CancellationToken cancellationToken)
        {
            await using var connection = await _connectionFactory.CreateAsync(cancellationToken);
            await using var command = new NpgsqlCommand(UserQueries.FindByIdQuery, connection)
            {
                Parameters =
                {
                    new NpgsqlParameter<Guid>("id", id)
                }
            };

            await using var reader = await command.ExecuteReaderAsync(CommandBehavior.SingleRow, cancellationToken);
            return await ReadUserAsync(reader, cancellationToken);
        }

        public async Task<User?> FindByNameOrDefaultAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            await using var connection = await _connectionFactory.CreateAsync(cancellationToken);
            await using var command = new NpgsqlCommand(UserQueries.FindByNameQuery, connection)
            {
                Parameters =
                {
                    new NpgsqlParameter<string>("normalized_user_name", normalizedUserName)
                }
            };

            await using var reader = await command.ExecuteReaderAsync(CommandBehavior.SingleRow, cancellationToken);
            return await ReadUserAsync(reader, cancellationToken);
        }

        public async Task<User?> FindByEmailOrDefaultAsync(string normalizedEmail, CancellationToken cancellationToken)
        {
            await using var connection = await _connectionFactory.CreateAsync(cancellationToken);
            await using var command = new NpgsqlCommand(UserQueries.FindByEmailQuery, connection)
            {
                Parameters =
                {
                    new NpgsqlParameter<string>("normalized_email", normalizedEmail)
                }
            };

            await using var reader = await command.ExecuteReaderAsync(CommandBehavior.SingleRow, cancellationToken);
            return await ReadUserAsync(reader, cancellationToken);
        }

        public async Task<bool> ExistsAsync(UserFilter userFilter, CancellationToken cancellationToken)
        {
            await using var connection = await _connectionFactory.CreateAsync(cancellationToken);
            await using var command = new NpgsqlCommand(UserQueries.Exists, connection)
            {
                Parameters =
                {
                    new NpgsqlParameter<string?>("normalized_user_name", userFilter.UserName?.Normalize().ToUpperInvariant()),
                    new NpgsqlParameter<string?>("normalized_email", userFilter.Email?.Normalize().ToUpperInvariant())
                }
            };

            await using var reader = await command.ExecuteReaderAsync(CommandBehavior.SingleRow, cancellationToken);
            return await reader.ReadAsync(cancellationToken) && reader.GetBoolean(0);
        }

        public async Task<Guid?> CreateAsync(User user, CancellationToken cancellationToken)
        {
            await using var connection = await _connectionFactory.CreateAsync(cancellationToken);
            await using var command = new NpgsqlCommand(UserQueries.Create, connection)
            {
                Parameters =
                {
                    new NpgsqlParameter<Guid>("id", user.Id),
                    new NpgsqlParameter<string>("user_name", user.UserName!),
                    new NpgsqlParameter<string>("normalized_user_name", user.NormalizedUserName!),
                    new NpgsqlParameter<string>("email", user.Email!),
                    new NpgsqlParameter<string>("normalized_email", user.NormalizedEmail!),
                    new NpgsqlParameter<string>("password_hash", user.PasswordHash!),
                }
            };

            await using var reader = await command.ExecuteReaderAsync(CommandBehavior.SingleRow, cancellationToken);
            return await reader.ReadAsync(cancellationToken) ? reader.GetFieldValue<Guid?>(0) : default;
        }

        public async Task UpdateAsync(User user, CancellationToken cancellationToken)
        {
            await using var connection = await _connectionFactory.CreateAsync(cancellationToken);
            await using var command = new NpgsqlCommand(UserQueries.Update, connection)
            {
                Parameters =
                {
                    new NpgsqlParameter<Guid>("id", user.Id),
                    new NpgsqlParameter<string?>("user_name", user.UserName),
                    new NpgsqlParameter<string?>("normalized_user_name", user.NormalizedUserName),
                    new NpgsqlParameter<string?>("email", user.Email),
                    new NpgsqlParameter<string?>("normalized_email", user.NormalizedEmail),
                    new NpgsqlParameter<string?>("phone_number", user.PhoneNumber),
                    new NpgsqlParameter<bool>("phone_number_confirmed", user.PhoneNumberConfirmed),
                    new NpgsqlParameter<string?>("password_hash", user.PasswordHash),
                }
            };

          var result =  await command.ExecuteNonQueryAsync(cancellationToken);
          if (result is -1 or 0)
              throw new Exception($"Could not update user with id = {user.Id}");
        }

        public async Task DeleteAsync(Guid id, CancellationToken cancellationToken)
        {
            await using var connection = await _connectionFactory.CreateAsync(cancellationToken);
            await using var command = new NpgsqlCommand(UserQueries.Delete, connection)
            {
                Parameters =
                {
                    new NpgsqlParameter<Guid>("id", id),
                }
            };

           var result = await command.ExecuteNonQueryAsync(cancellationToken);
           if (result is -1 or 0)
               throw new Exception($"Could not delete user with id = {id}");
        }

        private static async Task<User?> ReadUserAsync(NpgsqlDataReader reader, CancellationToken cancellationToken)
        {
            return !await reader.ReadAsync(cancellationToken)
                ? null
                : new User
                {
                    Id = reader.GetFieldValue<Guid>(0),
                    UserName = reader.GetFieldValue<string>(1),
                    Email = reader.GetFieldValue<string>(2),
                    PhoneNumber = reader.GetNullableString(3),
                    PasswordHash = reader.GetFieldValue<string>(4)
                };
        }
    }
}