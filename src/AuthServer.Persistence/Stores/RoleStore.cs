﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AuthServer.Domain;
using AuthServer.Persistence.Repositories;
using Microsoft.AspNetCore.Identity;

namespace AuthServer.Persistence.Stores
{
    public class RoleStore : IRoleStore<Role>
    {
        private readonly IRoleRepository _roleRepository;

        public RoleStore(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        public Task<Role> FindByIdAsync(string roleId, CancellationToken cancellationToken)
        {
            return _roleRepository.FindByIdOrDefaultAsync(Guid.Parse(roleId), cancellationToken);
        }

        public Task<Role> FindByNameAsync(string normalizedRoleName, CancellationToken cancellationToken)
        {
            return _roleRepository.FindByNameOrDefaultAsync(normalizedRoleName, cancellationToken);
        }

        public async Task<IdentityResult> CreateAsync(Role role, CancellationToken cancellationToken)
        {
            if (role is null)
                throw new ArgumentNullException(nameof(role), "Cannot create role");

            var roleId = await _roleRepository.CreateAsync(role, cancellationToken);

            return roleId != default
                ? IdentityResult.Success
                : IdentityResult.Failed();
        }

        public async Task<IdentityResult> UpdateAsync(Role role, CancellationToken cancellationToken)
        {
            if (role is null)
                throw new ArgumentNullException(nameof(role), "Cannot update role");

            await _roleRepository.UpdateAsync(role, cancellationToken);

            return IdentityResult.Success;
        }

        public async Task<IdentityResult> DeleteAsync(Role role, CancellationToken cancellationToken)
        {
            if (role is null)
                throw new ArgumentNullException(nameof(role), "Cannot delete user");

            await _roleRepository.DeleteAsync(role.Id, cancellationToken);

            return IdentityResult.Success;
        }

        public Task<string> GetRoleIdAsync(Role role, CancellationToken cancellationToken)
        {
            return Task.FromResult(role?.Id.ToString() ?? throw new ArgumentNullException(nameof(role), "Cannot get role id"));
        }

        public Task<string> GetRoleNameAsync(Role role, CancellationToken cancellationToken)
        {
            return Task.FromResult(role?.Name ?? throw new ArgumentNullException(nameof(role), "Cannot get role name"));
        }

        public Task SetRoleNameAsync(Role role, string roleName, CancellationToken cancellationToken)
        {
            if (role is null || string.IsNullOrWhiteSpace(roleName))
                throw new ArgumentNullException(nameof(role), "Cannot set role name");

            role.Name = roleName;
            return Task.CompletedTask;
        }

        public Task<string> GetNormalizedRoleNameAsync(Role role, CancellationToken cancellationToken)
        {
            return Task.FromResult(role?.NormalizedName ?? throw new ArgumentNullException(nameof(role), "Cannot get normalized role name"));
        }

        public Task SetNormalizedRoleNameAsync(Role role, string normalizedName, CancellationToken cancellationToken)
        {
            if (role is null || string.IsNullOrWhiteSpace(normalizedName))
                throw new ArgumentNullException(nameof(normalizedName), "Cannot set normalized role name");

            role.NormalizedName = normalizedName;
            return Task.CompletedTask;
        }

        public void Dispose()
        {
        }
    }
}