﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AuthServer.Domain;
using AuthServer.Persistence.Repositories;
using Microsoft.AspNetCore.Identity;

namespace AuthServer.Persistence.Stores
{
    public class UserStore :
        IUserStore<User>,
        IUserPasswordStore<User>,
        IUserEmailStore<User>,
        IUserPhoneNumberStore<User>
    {
        private readonly IUserRepository _userRepository;

        public UserStore(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public Task<string> GetUserIdAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user?.Id.ToString() ?? throw new ArgumentNullException(nameof(user), "Cannot get id"));
        }

        public Task<string> GetUserNameAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user?.UserName ?? throw new ArgumentNullException(nameof(user), "Cannot get username"));
        }

        public Task SetUserNameAsync(User user, string userName, CancellationToken cancellationToken)
        {
            if (user is null || string.IsNullOrWhiteSpace(userName))
                throw new ArgumentNullException(nameof(userName), "UserName cannot be null or empty");

            user.UserName = userName;
            return Task.CompletedTask;
        }

        public Task SetNormalizedUserNameAsync(User user, string normalizedName, CancellationToken cancellationToken)
        {
            if (user is null || string.IsNullOrWhiteSpace(normalizedName))
                throw new ArgumentNullException(nameof(normalizedName), "Normalized UserName cannot be null or empty");

            user.NormalizedUserName = normalizedName;
            return Task.CompletedTask;
        }

        public Task<string> GetNormalizedUserNameAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user?.NormalizedUserName ?? throw new ArgumentNullException(nameof(user), "Cannot get normalized username"));
        }

        public async Task<IdentityResult> CreateAsync(User user, CancellationToken cancellationToken)
        {
            if (user is null)
                throw new ArgumentNullException(nameof(user), "Cannot create user");

            var userId = await _userRepository.CreateAsync(user, cancellationToken);

            return userId != default
                ? IdentityResult.Success
                : IdentityResult.Failed();
        }

        public async Task<IdentityResult> UpdateAsync(User user, CancellationToken cancellationToken)
        {
            if (user is null)
                throw new ArgumentNullException(nameof(user), "Cannot update user");

            await _userRepository.UpdateAsync(user, cancellationToken);

            return IdentityResult.Success;
        }

        public async Task<IdentityResult> DeleteAsync(User user, CancellationToken cancellationToken)
        {
            if (user is null)
                throw new ArgumentNullException(nameof(user), "Cannot delete user");

            await _userRepository.DeleteAsync(user.Id, cancellationToken);
            return IdentityResult.Success;
        }

        public Task<User> FindByIdAsync(string userId, CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(userId))
                throw new ArgumentNullException(nameof(userId), "Cannot find user by id");

            return _userRepository.FindByIdOrDefaultAsync(Guid.Parse(userId), cancellationToken)!;
        }

        public Task<User> FindByNameAsync(string normalizedUserName, CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(normalizedUserName))
                throw new ArgumentNullException(nameof(normalizedUserName), "Cannot find user by name");

            return _userRepository.FindByNameOrDefaultAsync(normalizedUserName, cancellationToken)!;
        }

        public Task SetPasswordHashAsync(User user, string passwordHash, CancellationToken cancellationToken)
        {
            if (user is null || string.IsNullOrWhiteSpace(passwordHash))
                throw new ArgumentNullException(nameof(user), "Cannot set password");

            user.PasswordHash = passwordHash;
            return Task.CompletedTask;
        }

        public Task<string> GetPasswordHashAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user?.PasswordHash ?? throw new ArgumentNullException(nameof(user), "Cannot get password"));
        }

        public Task<bool> HasPasswordAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(string.IsNullOrWhiteSpace(user.PasswordHash));
        }

        public Task SetEmailAsync(User user, string email, CancellationToken cancellationToken)
        {
            if (user is null || string.IsNullOrWhiteSpace(email))
                throw new ArgumentNullException(nameof(user), "Cannot set email");

            user.Email = email;
            return Task.CompletedTask;
        }

        public Task<string> GetEmailAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user?.Email ?? throw new ArgumentNullException(nameof(user), "Cannot get email"));
        }

        public Task<bool> GetEmailConfirmedAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user?.EmailConfirmed ?? throw new ArgumentNullException(nameof(user), "Cannot get email confirmed"));
        }

        public Task SetEmailConfirmedAsync(User user, bool confirmed, CancellationToken cancellationToken)
        {
            if (user is null)
                throw new ArgumentNullException(nameof(user), "Cannot set email confirmed");

            user.EmailConfirmed = confirmed;
            return Task.CompletedTask;
        }

        public Task<User> FindByEmailAsync(string normalizedEmail, CancellationToken cancellationToken)
        {
            if (string.IsNullOrWhiteSpace(normalizedEmail))
                throw new ArgumentNullException(nameof(normalizedEmail), "Cannot find my email");

            return _userRepository.FindByEmailOrDefaultAsync(normalizedEmail, cancellationToken)!;
        }

        public Task<string> GetNormalizedEmailAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user?.NormalizedEmail ?? throw new ArgumentNullException(nameof(user), "Cannot get email"));
        }

        public Task SetNormalizedEmailAsync(User user, string normalizedEmail, CancellationToken cancellationToken)
        {
            if (user is null || string.IsNullOrWhiteSpace(normalizedEmail))
                throw new ArgumentNullException(nameof(user), "Cannot set email");

            user.NormalizedEmail = normalizedEmail;
            return Task.CompletedTask;
        }

        public Task SetPhoneNumberAsync(User user, string phoneNumber, CancellationToken cancellationToken)
        {
            if (user is null || string.IsNullOrWhiteSpace(phoneNumber))
                throw new ArgumentNullException(nameof(user), "Cannot set phone number");

            user.PhoneNumber = phoneNumber;
            return Task.CompletedTask;
        }

        public Task<string> GetPhoneNumberAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user?.PhoneNumber ?? throw new ArgumentNullException(nameof(user), "Cannot get phone number"));
        }

        public Task<bool> GetPhoneNumberConfirmedAsync(User user, CancellationToken cancellationToken)
        {
            return Task.FromResult(user?.PhoneNumberConfirmed ?? throw new ArgumentNullException(nameof(user), "Cannot get phone number confirmed"));
        }

        public Task SetPhoneNumberConfirmedAsync(User user, bool confirmed, CancellationToken cancellationToken)
        {
            if (user is null)
                throw new ArgumentNullException(nameof(user), "Cannot set phone number confirmed");

            user.PhoneNumberConfirmed = confirmed;
            return Task.CompletedTask;
        }

        public void Dispose()
        {
        }
    }
}