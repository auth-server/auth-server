﻿namespace AuthServer.Web.Configuration;

public class IdentityConfiguration
{
    public int? RequiredLength { get; set; }
    public bool? RequireDigit { get; set; }
    public bool? RequireNonAlphanumeric { get; set; }
    public bool? RequireUppercase { get; set; }
    public bool? RequireLowercase { get; set; }
}