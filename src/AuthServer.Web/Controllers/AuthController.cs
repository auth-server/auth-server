﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AuthServer.Domain;
using AuthServer.Persistence.Repositories;
using AuthServer.Users;
using AuthServer.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace AuthServer.Web.Controllers
{
    [Route("auth")]
    public class AuthController : Controller
    {
        private readonly IUserRepository _userRepository;
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;

        public AuthController(SignInManager<User> signInManager, IUserRepository userRepository, UserManager<User> userManager)
        {
            _signInManager = signInManager;
            _userRepository = userRepository;
            _userManager = userManager;
        }

        [HttpGet("login")]
        public IActionResult Login([FromQuery] string returnUrl)
        {
            ViewData["Title"] = "Login";
            return View(new LoginViewModel(returnUrl));
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(LoginViewModel vm)
        {
            var result = await _signInManager.PasswordSignInAsync(vm.UserName, vm.Password, true, false);

            if (result.Succeeded)
                return Redirect(vm.ReturnUrl ?? "/home");

            return View(new LoginViewModel(vm.ReturnUrl));
        }

        [HttpGet("signup")]
        public IActionResult SignUp([FromQuery] string returnUrl)
        {
            ViewData["Title"] = "Signup";
            return View(new SignUpViewModel(returnUrl));
        }

        [HttpPost("signup")]
        public async Task<IActionResult> SignUp(SignUpViewModel viewModel, CancellationToken cancellationToken)
        {
            var errors = await ValidateSignUp(viewModel, cancellationToken);

            if (errors.Any())
                return View(new SignUpViewModel(viewModel.ReturnUrl) { Errors = errors });

            var user = new User
            {
                Id = Guid.NewGuid(),
                UserName = viewModel.UserName,
                Email = viewModel.Email,
                PhoneNumber = viewModel.PhoneNumber
            };

            var result = await _userManager.CreateAsync(user, viewModel.Password);

            if (result.Succeeded && !string.IsNullOrWhiteSpace(viewModel.ReturnUrl))
                return Redirect(viewModel.ReturnUrl);

            return View(new SignUpViewModel(viewModel.ReturnUrl) { Errors = result.Errors.Select(x => x.Description).ToList() });
        }

        [HttpGet("authorized")]
        [Authorize(AuthenticationSchemes = "Identity.Application")]
        public IActionResult Auth()
        {
           return Ok(new { Haha = "haha" });
        }

        /// <summary>
        /// Validates signup view model 
        /// </summary>
        /// <returns> List of errors if model is not valid otherwise empty list </returns>
        private async ValueTask<IReadOnlyList<string>> ValidateSignUp(SignUpViewModel viewModel, CancellationToken cancellationToken)
        {
            var errors = new List<string>(4);

            if (string.IsNullOrWhiteSpace(viewModel.UserName))
                errors.Add("Username cannot be empty");

            // TODO: Add check for valid email
            if (string.IsNullOrWhiteSpace(viewModel.Email))
                errors.Add("Email cannot be empty");

            if (string.IsNullOrWhiteSpace(viewModel.Password))
                errors.Add("Password cannot be empty");

            if (viewModel.Password != viewModel.PasswordConfirm)
                errors.Add("Password and password confirmation are not the same");

            if (errors.Any())
                return errors;

            var filter = new UserFilter(viewModel.UserName, viewModel.Email);
            var exists = await _userRepository.ExistsAsync(filter, cancellationToken);
            if (exists)
                errors.Add("User with the same user name or email already exists");

            return errors;
        }
    }
}