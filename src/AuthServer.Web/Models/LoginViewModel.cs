﻿namespace AuthServer.Web.Models
{
    public record LoginViewModel(string ReturnUrl)
    {
        public string? UserName { get; init; }
        
        public string? Password { get; init; }
    }
}