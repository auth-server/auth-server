﻿using System;
using System.Collections.Generic;

namespace AuthServer.Web.Models
{
    public record SignUpViewModel(string ReturnUrl)
    {
        public string? UserName { get; set; }
        public string? Email { get; set; }
        public string? PhoneNumber { get; set; }
        public string? Password { get; set; }
        public string? PasswordConfirm { get; set; }

        public IReadOnlyList<string> Errors { get; set; } = Array.Empty<string>();
    }
}