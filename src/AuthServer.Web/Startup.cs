using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthServer.Domain;
using AuthServer.Infrastructure;
using AuthServer.Persistence.Migrations;
using AuthServer.Persistence.Repositories;
using AuthServer.Persistence.Stores;
using AuthServer.Web.Configuration;
using FluentMigrator.Runner;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace AuthServer.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            
            services.Configure<IdentityOptions>(Configuration.GetSection("IdentityConfiguration"));
            
            services
                .AddOptions<NpgsqlOptions>()
                .Configure<IConfiguration>((options, configuration) => options.ConnectionString = configuration.GetValue<string>("Db:Master"));
            
            services.AddFluentMigratorCore()
                .ConfigureRunner(builder =>
                {
                    builder.AddPostgres();
                    builder.ScanIn(typeof(Initial).Assembly);
                    builder.WithGlobalConnectionString(Configuration.GetSection("Db:Master").Value);
                });

            services
                .AddScoped<IUserRepository, UserRepository>()
                .AddScoped<IRoleRepository, RoleRepository>()
                .AddSingleton<INpgsqlConnectionFactory, NpgsqlConnectionFactory>();
            
            services.AddIdentity<User, Role>(opts =>
                {
                    var identityConfiguration = new IdentityConfiguration();
                    Configuration.Bind("IdentityConfiguration", identityConfiguration);
                    opts.Password.RequiredLength = identityConfiguration.RequiredLength ?? opts.Password.RequiredLength;
                    opts.Password.RequireDigit = identityConfiguration.RequireDigit ?? opts.Password.RequireDigit;
                    opts.Password.RequireNonAlphanumeric = identityConfiguration.RequireNonAlphanumeric ?? opts.Password.RequireNonAlphanumeric;
                    opts.Password.RequireUppercase = identityConfiguration.RequireUppercase ?? opts.Password.RequireUppercase;
                    opts.Password.RequireLowercase = identityConfiguration.RequireLowercase ?? opts.Password.RequireLowercase;
                })
                .AddUserStore<UserStore>()
                .AddRoleStore<RoleStore>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}