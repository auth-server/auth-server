﻿namespace AuthServer.Users
{
    public record UserFilter(string? UserName, string? Email);
}